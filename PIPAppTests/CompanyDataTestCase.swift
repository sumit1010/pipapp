//
//  EmployeeTestCase.swift
//  PIPAppTests
//
//  Created by Sumit Vishwakarma on 27/07/23.
//

import XCTest
import CoreData
@testable import PIPApp

class CompanyDataTestCase: XCTestCase {
    
    let companyDataManager = CompanyDataManager()
    let manager = CompanyManager()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_Init_CoreDataManager() {
        XCTAssertNotNil(companyDataManager)
    }
    
    func test_CoreDataStackInitialization() {
        let coreDataStack = PersistentStorage.shared.persistentContainer
        XCTAssertNotNil(coreDataStack)
    }
    
    func test_Create_Company() {
        let expectation = self.expectation(description: "createEmployee")
        let  isSaved = manager.createPerson(record: Company(companyId: UUID(), companyName: "Test", employeeData: []))
        expectation.fulfill()
        XCTAssertNotNil(isSaved)
        XCTAssertEqual(isSaved, true)
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func test_FetchAllCompanyRecrods() {
        let expectation = self.expectation(description: "fetchEmployees")
        let results = manager.getSavedCompanyData()
        expectation.fulfill()
        XCTAssertEqual(results?.count != 0, results!.count > 0)
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func test_DeleteEmployee() {
        let isDeleted = self.manager.deleteEmployeeWith(id: UUID())
        XCTAssertEqual(isDeleted, true)
    }
}

 
