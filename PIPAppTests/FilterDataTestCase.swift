//
//  FilterDataTestCase.swift
//  PIPAppTests
//
//  Created by Sumit Vishwakarma on 02/08/23.
//

import XCTest
import CoreData
@testable import PIPApp

final class FilterDataTestCase: XCTestCase {
    
    let companyDataManager = CompanyDataManager()
    let manager = CompanyManager()
    var filterVC: FilterVC!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyVC = UIStoryboard(name: "Main", bundle: nil)
        filterVC = storyVC.instantiateViewController(withIdentifier: Constants.filterVC) as? FilterVC
        filterVC.loadViewIfNeeded()
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_Init_CoreDataManager() {
        XCTAssertNotNil(companyDataManager)
    }
    
    func test_CoreDataStackInitialization() {
        let coreDataStack = PersistentStorage.shared.persistentContainer
        XCTAssertNotNil(coreDataStack)
    }
    
    func test_allTextFieldValidationForKeyBoardType() throws {
        let nameTextField      = try XCTUnwrap(filterVC.textFieldNameOrPart, "nameTextFiled is not connected")
        let minSalaryTextField = try XCTUnwrap(filterVC.textFieldMinSalary, "minSalaryTextField is not connected")
        let maxSalaryTextField = try XCTUnwrap(filterVC.textFieldMaxSalary, "maxSalaryTextField is not connected")
        
        XCTAssertEqual(nameTextField.keyboardType, .default)
        XCTAssertEqual(minSalaryTextField.keyboardType, .numberPad)
        XCTAssertEqual(maxSalaryTextField.keyboardType, .numberPad)
    }
    
    func test_filterEmployeeNameContainsTo() {
        let allCompanyData = manager.getSavedCompanyData()
        let employeeName = getEmployeeName()
        let expectation = self.expectation(description: "filterEmployeeNameContainsTo")
        
        for allEmployee in allCompanyData ?? [] {
            if allEmployee.employeeData.count != 0 {
                let employee = allEmployee.employeeData.filter({$0.employeeName.lowercased().contains(employeeName) })
                if employee.count != 0 {
                    for data in employee {
                        if data.employeeName.contains(employeeName) {
                            XCTAssertTrue(true)
                            expectation.fulfill()
                        }
                    }
                }
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_filterEmployeeNameEqualsTo() {
        let expectation = self.expectation(description: "filterEmployeeNameEqualsTo")
        let employeeName = getEmployeeName()
        let allCompanyData = manager.getSavedCompanyData()
        for allEmployee in allCompanyData ?? [] {
            if allEmployee.employeeData.count != 0 {
                let employee = allEmployee.employeeData.filter({$0.employeeName.lowercased().contains(employeeName) })
                if employee.count != 0 {
                    for data in employee {
                        if data.employeeName == employeeName {
                            XCTAssertTrue(true)
                            expectation.fulfill()
                        }
                    }
                }
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_filterEmployeeMinimumSalary() {
        let expectation = self.expectation(description: "employeeMinSalary")
        let employeeSalary = getEmployeeSalary()
        let allCompanyData = manager.getSavedCompanyData()
        for allEmployee in allCompanyData ?? [] {
            if allEmployee.employeeData.count != 0 {
                let employee = allEmployee.employeeData.filter({$0.employeeSalary <= Float(employeeSalary)})
                if employee.count != 0 {
                    for data in employee {
                        if data.employeeSalary <= Float(employeeSalary) {
                            XCTAssertTrue(true)
                            expectation.fulfill()
                        }
                    }
                }
            }
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    //    func test_filterEmployeeMaxSalary() {
    //        let expectation = self.expectation(description: "employeeMinSalary")
    //        let employeeSalary = getEmployeeSalary()
    //        let allCompanyData = manager.getSavedCompanyData()
    //        for allEmployee in allCompanyData ?? [] {
    //            if allEmployee.employeeData.count != 0 {
    //                let employee = allEmployee.employeeData.filter({$0.employeeSalary >= Float(employeeSalary)})
    //                if employee.count != 0 {
    //                    for data in employee {
    //                        if data.employeeSalary > Float(employeeSalary) {
    //                            XCTAssertTrue(true)
    //                            expectation.fulfill()
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        waitForExpectations(timeout: 10, handler: nil)
    //    }
    
    func getEmployeeName() -> String {
        let allCompanyData = manager.getSavedCompanyData()
        if let companyData = allCompanyData, companyData.count != 0 {
            let employeeData = companyData.first?.employeeData ?? []
            if employeeData.count != 0 {
                if let name = employeeData.first?.employeeName {
                    return name
                }
            }
        }
        return ""
    }
    
    func getEmployeeSalary() -> Float {
        let allCompanyData = manager.getSavedCompanyData()
        if let companyData = allCompanyData, companyData.count != 0 {
            let employeeData = companyData.first?.employeeData ?? []
            if employeeData.count != 0 {
                if let salary = employeeData.first?.employeeSalary {
                    return salary
                }
            }
        }
        return 0.0
    }
}
