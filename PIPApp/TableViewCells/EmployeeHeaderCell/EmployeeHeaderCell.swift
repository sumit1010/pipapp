//
//  EmployeeHeaderCell.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 19/07/23.
//

import UIKit

protocol EmployeeHeaderCellDelegate {
    func getSelectedCompanySectionValue(sectionValue: Int)
}

class EmployeeHeaderCell: UITableViewCell {
    @IBOutlet var lableHeaderTitle : UILabel!
    @IBOutlet var buttonAddEmployee : UIButton!
    
    
    var delegate: EmployeeHeaderCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func buttonAddEmployeeOnSectionAction(_ sender : UIButton){
        delegate?.getSelectedCompanySectionValue(sectionValue: sender.tag)
    }
}
