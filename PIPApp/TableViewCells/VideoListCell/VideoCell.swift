//
//  VideoCell.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 08/08/23.
//

import UIKit
import Foundation
import AVFoundation

class VideoCell: UITableViewCell {
    
    @IBOutlet var imageViewThumbnail: UIImageView!
    @IBOutlet var labelTitle : UILabel!
    @IBOutlet var labelSubTitle: UILabel!
    @IBOutlet var btnDownload: UIButton!
    @IBOutlet var activityLoaderInCell: UIActivityIndicatorView!
    @IBOutlet var progressBar: UIProgressView!
    var player = AVPlayer()
    
    
    func configuraCell(videoMetaData: Video, index: Int , videoListViewModel: VideoListViewModel) {
        
        labelTitle.text = videoMetaData.title
        labelSubTitle.text = videoMetaData.subtitle
        btnDownload.tag = index
        progressBar.tag = index
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedOnThumbnail))
        tap.view?.tag = index
        self.imageViewThumbnail.addGestureRecognizer(tap)
        self.imageViewThumbnail.isUserInteractionEnabled = true
        self.imageViewThumbnail.tag = index
        
        if let videoUrl = videoMetaData.sources.first {
            
            if videoUrl.contains("http") {
                DispatchQueue.global(qos: .background).async {
                    self.showHideLoader(isShowLoader: false)
                    do {
                        guard let imageUrl = URL(string: Constants.baseUrlForThummnailLoading + videoMetaData.thumb) else { return }
                        let imageData = try Data.init(contentsOf: imageUrl)
                        DispatchQueue.main.async {
                            if let thumbnailImage = UIImage(data: imageData) {
                                self.imageViewThumbnail.image = thumbnailImage
                                self.showHideLoader(isShowLoader: true)
                            }
                        }
                    } catch {
                        print(error)
                        self.showHideLoader(isShowLoader: false)
                    }
                }
            } else {
                self.showHideLoader(isShowLoader: true)
                let videoURL = URL(string: videoUrl)
                if let videoURL {
                    player = AVPlayer(url: videoURL)
                    let playerLayer = AVPlayerLayer(player: player)
                    playerLayer.frame = self.imageViewThumbnail.bounds
                    playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    self.imageViewThumbnail.layer.addSublayer(playerLayer)
                }
            }
        }
    }
    
    @objc func tappedOnThumbnail() {
        if player.rate != 0 {
            player.pause()
        } else {
            player.play()
        }
    }
    
    func showHideLoader(isShowLoader: Bool) {
        DispatchQueue.main.async {
            if isShowLoader {
                self.activityLoaderInCell.isHidden = isShowLoader
            } else {
                self.activityLoaderInCell.isHidden = isShowLoader
                self.activityLoaderInCell.startAnimating()
            }
        }
    }
 
}
