//
//  EmployeeCell.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 18/07/23.
//

import UIKit

protocol EmployeeCellDelegate {
    func didEmployeeDeleted()
}

class EmployeeCell: UITableViewCell {
    
    @IBOutlet weak var labelEmployeeName: UILabel!
    @IBOutlet weak var labelEmployeeSalary: UILabel!
    @IBOutlet weak var labelCompanyName: UILabel!
    @IBOutlet weak var buttonDelete: UIButton!
    @IBOutlet var lableName : UILabel!
    @IBOutlet var lableSalary : UILabel!
    
    private let manager = CompanyManager()
    var delegate: EmployeeCellDelegate?
    var employee: Employee?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func buttonDeleteAction(_ sender : UIButton){
        if let emp = employee {
            let isDeleted = self.manager.deleteEmployeeWith(id: emp.employeeId)
            if isDeleted {
                delegate?.didEmployeeDeleted()
            }
        }
    }
    
    func configureCell(employee: Employee) {
        
        self.lableName.text = StringConstant.name.localizeString()
        self.lableSalary.text = StringConstant.salary.localizeString()
        self.buttonDelete.setTitle(StringConstant.delete.localizeString(), for: .normal)
        
        self.employee = employee
        self.labelEmployeeName.text = employee.employeeName
        self.labelEmployeeSalary.text = String(employee.employeeSalary)
    }
}
