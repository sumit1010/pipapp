//
//  VideoListVCViewModel.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 08/08/23.
//

import Foundation
import AVFoundation
import UIKit
import AVFAudio

class VideoListViewModel {
    
    var arrayOfVideosData: EmployeeBinder<[Video]> = EmployeeBinder([])
    var isLoading: EmployeeBinder<Bool> = EmployeeBinder(true)
    var videoUrlString: EmployeeBinder<String> = EmployeeBinder("")
    var progress: Float = 0
    var progressPecentage = ""
    var videoFileName = ""
    var documentDirectoryPath = ""
    
    
    init() {
        getAllVideosData()
    }
    
    func getAllVideosData() {
        let paths = Bundle.main.paths(forResourcesOfType: "json", inDirectory: "")
        let mockPaths = paths.filter { $0.components(separatedBy: "/").last?.hasPrefix("videos") == true }
        if mockPaths.count != 0, let first = mockPaths.first {
            guard let data = try? Data(contentsOf: URL(fileURLWithPath: first)), let mockJSON = try? JSONDecoder().decode(VideoListModel.self, from: data)
            else { return }
            if let videosData = mockJSON.categories.first?.videos {
                self.arrayOfVideosData.value = videosData
                if self.arrayOfVideosData.value.count != 0 {
                    retriveAllSavedVideos()
                }
            }
        }
    }
    
    func retriveAllSavedVideos() {
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath = paths.first {
            let videoURLPath = URL(fileURLWithPath: dirPath)
            do {
                let fileManager = FileManager.default
                let savedVideosArray = try fileManager.contentsOfDirectory(at: videoURLPath, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                if savedVideosArray.count != 0 {
                    for index in 0..<self.arrayOfVideosData.value.count {
                        let title = self.arrayOfVideosData.value[index].title
                        for vidoeUrl in savedVideosArray {
                            if vidoeUrl.relativePath.contains(title) {
                                self.arrayOfVideosData.value[index].sources[0] = vidoeUrl.absoluteString
                            }
                        }
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
}

extension String {
    func containsValue(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
