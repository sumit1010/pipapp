//
//  VideoListVC.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 08/08/23.
//

import UIKit
import Foundation
import AVFoundation

class VideoListVC: UIViewController {
    
    @IBOutlet var tableViewOfVideos: UITableView!
    @IBOutlet var activityLoader: UIActivityIndicatorView!
    var videoListViewModel: VideoListViewModel = VideoListViewModel()
    var rowNUmber: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityLoader.isHidden = false
        bindVideoListData()
    }
    
    func reloadTableView() {
        DispatchQueue.main.async {
            self.tableViewOfVideos.reloadData()
        }
    }
    
    func bindVideoListData() {
        self.videoListViewModel.arrayOfVideosData.bind {  arrayOfVideos in
            self.reloadTableView()
        }
        
        self.videoListViewModel.isLoading.bind {  isLoading in
            if !isLoading {
                DispatchQueue.main.async {
                    self.activityLoader.isHidden = true
                }
            }
        }
        
        self.videoListViewModel.videoUrlString.bind { url in
            
            if url != "" {
                let videoURL = URL(string: url)
                let player = AVPlayer(url: videoURL!)
                let playerLayer = AVPlayerLayer(player: player)
                playerLayer.frame = self.view.bounds
                self.view.layer.addSublayer(playerLayer)
                player.play()
            }
            
            
        }
        
    }
    
}


extension VideoListVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.videoListViewModel.arrayOfVideosData.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let videoCell = tableView.dequeueReusableCell(withIdentifier: Constants.videoCell, for: indexPath) as! VideoCell
        videoCell.configuraCell(videoMetaData: self.videoListViewModel.arrayOfVideosData.value[indexPath.row], index: indexPath.row, videoListViewModel: self.videoListViewModel)
        videoCell.btnDownload.addTarget(self, action: #selector(downloadButtonAction(sender:)), for: .touchUpInside)
        return videoCell
    }
    
    @objc func downloadButtonAction(sender : UIButton) {
        
        self.rowNUmber = sender.tag
        if let videoUrlString = self.videoListViewModel.arrayOfVideosData.value[sender.tag].sources.first  {
            self.videoListViewModel.videoFileName = self.videoListViewModel.arrayOfVideosData.value[sender.tag].title
            if let videoUrl = URL(string: videoUrlString) {
                print("Video ULR:- \(videoUrl)")
                self.downloadVideoByUrl(url: videoUrl)
            }
        }
    }
}

extension VideoListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}

extension VideoListVC: URLSessionDownloadDelegate {
    
    func downloadVideoByUrl(url: URL) {
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        let task = session.dataTask(with: url) { ( serverData, serverResponse, serverError) in
            guard serverError == nil else {
                return
            }
            let downloadTask = session.downloadTask(with: url)
            downloadTask.resume()
        }
        task.resume()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        let fileManager = FileManager.default
        let directoryPath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
        UserDefaults.saveDocumentDirectoryPath(path: directoryPath as String)
        let paths = directoryPath.appendingPathComponent(self.videoListViewModel.videoFileName + ".MOV")
        
        do {
            let videoData = try Data(contentsOf: location)
            fileManager.createFile(atPath: paths as String, contents: videoData, attributes: nil)
            print("Path:->\(paths)")
            self.videoListViewModel.retriveAllSavedVideos()
        } catch {
            print("Failed while saving video")
        }
    }
  
}





