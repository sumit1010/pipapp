
import Foundation
 
struct VideoListModel: Codable {
    let categories: [Category]
}

struct Category: Codable {
    let name: String
    let videos: [Video]
}

struct Video: Codable {
    let description: String
    var sources: [String]
    let thumb, title, subtitle: String
}

 


