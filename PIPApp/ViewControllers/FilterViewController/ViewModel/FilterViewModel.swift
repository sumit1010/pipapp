//
//  FilterViewModel.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 21/07/23.

import Foundation

class FilterViewModel {
    
    var arrayOfFileteredCompany: EmployeeBinder<[Company]> = EmployeeBinder([])
    var filterStringName:String = ""
    var filterStringMinSalary:Float = 0.0
    var filterStringMaxSalary:Float = 0.0
    var isNoFilter: Bool = false
    var isNameEqualtToOrContainsTo: Bool = false
    var isSalaryLessOrMoreThan: Bool = false
    var selectedTypeNoFilter:Int = -1
    var filterNameIndex:Int = -1
    var filterSalaryIndex:Int = -1
    
    init() {}
    
    func fetchFilteredCompanyRecords() {
        let manager = CompanyManager()
        let allCompanyData = manager.getSavedCompanyData()
        if self.isNoFilter {
            if allCompanyData?.count != 0 {
                self.arrayOfFileteredCompany.value = allCompanyData ?? []
            }
        } else {
            
            var filteredCompany: [Company] = []
            if isNameEqualtToOrContainsTo && isSalaryLessOrMoreThan {
                if filterNameIndex == 2 && filterSalaryIndex == 4 {
                    for allEmployee in allCompanyData ?? [] {
                        if allEmployee.employeeData.count != 0 {
                            let employee = allEmployee.employeeData.filter({$0.employeeName.lowercased() == filterStringName && $0.employeeSalary < Float(filterStringMinSalary) })
                            if employee.count != 0 {
                                filteredCompany.append(Company(companyId: allEmployee.companyId,
                                                               companyName: allEmployee.companyName,employeeData: employee))
                            }
                        }
                    }
                }
                
                if filterNameIndex == 2 && filterSalaryIndex == 5 {
                    for allEmployee in allCompanyData ?? [] {
                        if allEmployee.employeeData.count != 0 {
                            let employee = allEmployee.employeeData.filter({$0.employeeName.lowercased() == filterStringName && $0.employeeSalary > Float(filterStringMaxSalary) })
                            if employee.count != 0 {
                                filteredCompany.append(Company(companyId: allEmployee.companyId,
                                                               companyName: allEmployee.companyName,employeeData: employee))
                            }
                        }
                    }
                }
                
                if filterNameIndex == 3 && filterSalaryIndex == 4 {
                    for allEmployee in allCompanyData ?? [] {
                        if allEmployee.employeeData.count != 0 {
                            let employee = allEmployee.employeeData.filter({$0.employeeName.lowercased().contains(filterStringName)  && $0.employeeSalary < Float(filterStringMinSalary) })
                            if employee.count != 0 {
                                filteredCompany.append(Company(companyId: allEmployee.companyId,
                                                               companyName: allEmployee.companyName,employeeData: employee))
                            }
                        }
                    }
                }
                
                if filterNameIndex == 3 && filterSalaryIndex == 5 {
                    for allEmployee in allCompanyData ?? [] {
                        if allEmployee.employeeData.count != 0 {
                            let employee = allEmployee.employeeData.filter({$0.employeeName.lowercased().contains(filterStringName) && $0.employeeSalary > Float(filterStringMaxSalary) })
                            if employee.count != 0 {
                                filteredCompany.append(Company(companyId: allEmployee.companyId,
                                                               companyName: allEmployee.companyName,employeeData: employee))
                            }
                        }
                    }
                }
                
            } else {
                
                if isNameEqualtToOrContainsTo {
                    
                    if filterNameIndex == 2  {
                        for allEmployee in allCompanyData ?? [] {
                            if allEmployee.employeeData.count != 0 {
                                let employee = allEmployee.employeeData.filter({$0.employeeName.lowercased() == filterStringName })
                                if employee.count != 0 {
                                    filteredCompany.append(Company(companyId: allEmployee.companyId,
                                                                   companyName: allEmployee.companyName,employeeData: employee))
                                }
                            }
                        }
                    }
                    
                    if filterNameIndex == 3  {
                        for allEmployee in allCompanyData ?? [] {
                            if allEmployee.employeeData.count != 0 {
                                let employee = allEmployee.employeeData.filter({$0.employeeName.lowercased().contains(filterStringName) })
                                if employee.count != 0 {
                                    filteredCompany.append(Company(companyId: allEmployee.companyId,
                                                                   companyName: allEmployee.companyName,employeeData: employee))
                                }
                            }
                        }
                    }
                }
                
                if isSalaryLessOrMoreThan {
                    if filterSalaryIndex == 4  {
                        for allEmployee in allCompanyData ?? [] {
                            if allEmployee.employeeData.count != 0 {
                                let employee = allEmployee.employeeData.filter({$0.employeeSalary < filterStringMinSalary })
                                if employee.count != 0 {
                                    filteredCompany.append(Company(companyId: allEmployee.companyId,
                                                                   companyName: allEmployee.companyName,employeeData: employee))
                                }
                            }
                        }
                    }
                    
                    if filterSalaryIndex == 5  {
                        for allEmployee in allCompanyData ?? [] {
                            if allEmployee.employeeData.count != 0 {
                                let employee = allEmployee.employeeData.filter({$0.employeeSalary > filterStringMaxSalary })
                                if employee.count != 0 {
                                    filteredCompany.append(Company(companyId: allEmployee.companyId,
                                                                   companyName: allEmployee.companyName,employeeData: employee))
                                }
                            }
                        }
                    }
                }
            }
            self.arrayOfFileteredCompany.value = filteredCompany
        }
    }
}

