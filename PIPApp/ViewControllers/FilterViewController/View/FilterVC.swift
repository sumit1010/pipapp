//
//  FilterVC.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 18/07/23.
//

import UIKit

protocol FilterVCDelegate {
    func allFilteredCompanyData(filterDataArray: [Company],nameFilterIndex: Int , salaryFilterIndex: Int , name: String , minSalary: Float , maxSalary: Float)
}

class FilterVC: UIViewController {
    
    @IBOutlet weak var labelNameOfPart: UILabel!
    @IBOutlet weak var labelMinSalary: UILabel!
    @IBOutlet weak var labelMaxSalary: UILabel!
    @IBOutlet weak var buttonOk: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var textFieldNameOrPart: UITextField!
    @IBOutlet weak var textFieldMinSalary: UITextField!
    @IBOutlet weak var textFieldMaxSalary: UITextField!
    @IBOutlet var buttonNoFilter: UIButton!
    @IBOutlet var buttonNameContainsTo: UIButton!
    @IBOutlet var buttonNameEqualsTo: UIButton!
    @IBOutlet var buttonSalaryLessThan: UIButton!
    @IBOutlet var buttonSalaryMoreThan: UIButton!
    @IBOutlet var collectionForNamesButton: Array<UIButton>?
    @IBOutlet var collectionForSalaryButton: Array<UIButton>?
    @IBOutlet var collectionOfButtons: Array<UIButton>?
    var filterViewModel: FilterViewModel = FilterViewModel()
    var delegate: FilterVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bindFilteredCompanyArrayData()
        self.labelNameOfPart.text = StringConstant.nameOrPart.localizeString()
        self.labelMinSalary.text = StringConstant.minSalary.localizeString()
        self.labelMaxSalary.text = StringConstant.maxSalary.localizeString()
        self.buttonNoFilter.setTitle(StringConstant.noFilter.localizeString(), for: .normal)
        self.buttonNameEqualsTo.setTitle(StringConstant.nameEqualsTo.localizeString(), for: .normal)
        self.buttonNameContainsTo.setTitle(StringConstant.nameContainsTo.localizeString(), for: .normal)
        self.buttonSalaryLessThan.setTitle(StringConstant.salaryLessThan.localizeString(), for: .normal)
        self.buttonSalaryMoreThan.setTitle(StringConstant.salaryMoreThan.localizeString(), for: .normal)
        self.buttonOk.setTitle(StringConstant.okTitle.localizeString(), for: .normal)
        self.buttonCancel.setTitle(StringConstant.cancelFilter.localizeString(), for: .normal)
    }
    
    func bindFilteredCompanyArrayData() {
        self.filterViewModel.arrayOfFileteredCompany.bind { filteredCompanyDataArray in
            if filteredCompanyDataArray.count != 0 {
                self.delegate?.allFilteredCompanyData(filterDataArray: filteredCompanyDataArray,
                                                      nameFilterIndex: self.filterViewModel.filterNameIndex,
                                                      salaryFilterIndex: self.filterViewModel.filterSalaryIndex,
                                                      name: self.filterViewModel.filterStringName,
                                                      minSalary: self.filterViewModel.filterStringMinSalary,
                                                      maxSalary: self.filterViewModel.filterStringMaxSalary)
            }
        }
    }
    
    @IBAction func buttonOkActions(_ sender: UIButton) {
        self.dismiss(animated: true)
        self.filterViewModel.fetchFilteredCompanyRecords()
    }
    
    @IBAction func buttonCancelActions(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    
    @IBAction func buttonNoFilterAction(_ sender: UIButton) {
        self.filterViewModel.isNameEqualtToOrContainsTo = false
        self.filterViewModel.isSalaryLessOrMoreThan = false
        self.view.endEditing(true)
        if self.filterViewModel.isNoFilter {
            self.filterViewModel.isNoFilter = false
            buttonNoFilter.setImage(UIImage(named: Constants.uncheckImage), for: .normal)
        } else {
            self.filterViewModel.isNoFilter = true
            buttonNoFilter.setImage(UIImage(named: Constants.checkImage), for: .normal)
            buttonNameContainsTo.setImage(UIImage(named: Constants.uncheckImage), for: .normal)
            buttonNameEqualsTo.setImage(UIImage(named: Constants.uncheckImage), for: .normal)
            buttonSalaryLessThan.setImage(UIImage(named: Constants.uncheckImage), for: .normal)
            buttonSalaryMoreThan.setImage(UIImage(named: Constants.uncheckImage), for: .normal)
        }
    }
    
    @IBAction func buttonNamesAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.filterViewModel.isNameEqualtToOrContainsTo = true
        buttonNoFilter.setImage(UIImage(named: Constants.uncheckImage), for: .normal)
        guard let buttonNames = collectionForNamesButton else { return }
        if buttonNames.count != 0 {
            buttonNames.forEach { button in
                if button.tag == sender.tag {
                    button.setImage(UIImage(named: Constants.checkImage), for: .normal)
                    self.filterViewModel.filterNameIndex = button.tag
                    
                } else {
                    button.setImage(UIImage(named: Constants.uncheckImage), for: .normal)
                }
            }
        }
    }
    
    @IBAction func buttonSalaryAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.filterViewModel.isSalaryLessOrMoreThan = true
        buttonNoFilter.setImage(UIImage(named: Constants.uncheckImage), for: .normal)
        guard let buttonSalary = collectionForSalaryButton else { return }
        if buttonSalary.count != 0 {
            buttonSalary.forEach { button in
                if button.tag == sender.tag {
                    button.setImage(UIImage(named: Constants.checkImage), for: .normal)
                    self.filterViewModel.filterSalaryIndex = button.tag
                } else {
                    button.setImage(UIImage(named: Constants.uncheckImage), for: .normal)
                }
            }
        }
    }
}

//extension FilterVC: FilterViewModelDelegate {
//    func didReceivedFilteredCompanyFromDB(filterDataArray: [Company]) {
//        if filterDataArray.count != 0 {
//            delegate?.allFilteredCompanyData(filterDataArray: filterDataArray,
//                                             nameFilterIndex: self.filterViewModel.filterNameIndex,
//                                             salaryFilterIndex: self.filterViewModel.filterSalaryIndex,
//                                             name: self.filterViewModel.filterStringName,
//                                             minSalary: self.filterViewModel.filterStringMinSalary,
//                                             maxSalary: self.filterViewModel.filterStringMaxSalary)
//        }
//    }
//
//}

extension FilterVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        
        if textField == textFieldNameOrPart {
            self.filterViewModel.filterStringName = txtAfterUpdate.lowercased()
            guard let buttonsArray = collectionForNamesButton else { return true}
            for btn in buttonsArray {
                if btn.tag == 2 {
                    btn.setTitle("\(StringConstant.nameEqualsTo.localizeString()) \"\(txtAfterUpdate)\"" , for: .normal)
                }
                if btn.tag == 3 {
                    btn.setTitle("\(StringConstant.nameContainsTo.localizeString()) \"\(txtAfterUpdate)\"" , for: .normal)
                }
            }
        }
        
        if textField == textFieldMaxSalary {
            self.filterViewModel.filterStringMaxSalary = Float(txtAfterUpdate) ?? 0.0
            guard let buttonsArray = collectionForSalaryButton else { return true }
            for btn in buttonsArray {
                if btn.tag == 5 {
                    btn.setTitle("\(StringConstant.salaryMoreThan.localizeString()) \(txtAfterUpdate == "" ? "0.0" : txtAfterUpdate)" , for: .normal)
                }
            }
        }
        
        if textField == textFieldMinSalary {
            self.filterViewModel.filterStringMinSalary = Float(txtAfterUpdate) ?? 0.0
            guard let buttonsArray = collectionForSalaryButton else { return true }
            for btn in buttonsArray {
                if btn.tag == 4 {
                    btn.setTitle("\(StringConstant.salaryLessThan.localizeString()) \(txtAfterUpdate == "" ? "0.0" : txtAfterUpdate)" , for: .normal)
                }
            }
        }
        return true
    }
}


