//
//  EmployeeVC+ChangeLanguagePopUpDelegate.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 04/08/23.
//

import Foundation

extension EmployeeVC: ChangeLanguageDelegate {
    
    func didChangeLanguage() {
        self.labelOfEmployeeHeader.text = StringConstant.findAllEmployees.localizeString()
        self.buttonFilter.setTitle(StringConstant.filters.localizeString(), for: .normal)
        self.buttonClearData.setTitle(StringConstant.clearData.localizeString(), for: .normal)
        self.reloadTableView()
    }
}
