//
//  EmployeeVC+TableViewDelegate.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 20/07/23.
//

import Foundation
import UIKit

extension EmployeeVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.employeeHeaderCell) as! EmployeeHeaderCell
        cell.lableHeaderTitle.text = self.employeeViewModel.isFilterProcessExectutedByUser ? employeeViewModel.arrayOfFileteredCompany.value[section].companyName : employeeViewModel.arrayOfComapanyData.value[section].companyName
        cell.buttonAddEmployee.tag = section
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let name = self.employeeViewModel.arrayOfComapanyData.value[indexPath.section].employeeData[indexPath.row].employeeName
        print(name)
    }
}
