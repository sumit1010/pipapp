//
//  ViewController.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 17/07/23.
//

import UIKit

class EmployeeVC: UIViewController {
    
    // outlets of UI Objects
    @IBOutlet weak var tableViewEmployee: UITableView!
    @IBOutlet weak var labelOfEmployeeHeader: UILabel!
    @IBOutlet weak var buttonFilter: UIButton!
    @IBOutlet weak var buttonClearData: UIButton!
    var buttonAddCompany : UIButton!
    var buttonChangeLanguage : UIButton!
    var employeeViewModel: EmployeeViewModel = EmployeeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.employeeViewModel.fetchCompanyRecords()
        self.bindEmployeeViewModel()
    }
    
    //MARK: Configure userinterface With Left/Right Bar button
    func configureUI() {
        labelOfEmployeeHeader.text = StringConstant.findAllEmployees.localizeString()
        buttonAddCompany = UIButton()
        buttonAddCompany.frame = CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0)
        buttonAddCompany.setImage(UIImage(named: Constants.addImageName), for: .normal)
        buttonAddCompany.addTarget(self, action: #selector(addCompanyButtonAction), for: .touchUpInside)
        let barButton = UIBarButtonItem()
        barButton.customView = buttonAddCompany
        self.navigationItem.rightBarButtonItem = barButton
        
        buttonChangeLanguage = UIButton()
        buttonChangeLanguage.frame = CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0)
        buttonChangeLanguage.setImage(UIImage(named: Constants.moreImageName), for: .normal)
        buttonChangeLanguage.addTarget(self, action: #selector(changeLanguageAction), for: .touchUpInside)
        let barButton1 = UIBarButtonItem()
        barButton1.customView = buttonChangeLanguage
        self.navigationItem.leftBarButtonItem = barButton1
    }
    
    func bindEmployeeViewModel() {
        self.employeeViewModel.arrayOfComapanyData.bind { allCompanyData in
            self.reloadTableView()
        }
    }
    
    //MARK: Add company button action
    @objc func addCompanyButtonAction() {
        showPopupViewForAddCompany()
    }
    
    //MARK: Add company button action
    @objc func changeLanguageAction() {
        self.showPopupViewForChangeLanguage()
    }
    
    //MARK: Show Change Language PopUp
    private func showPopupViewForChangeLanguage(){
        
        if let bundle = (Bundle.main.loadNibNamed(Constants.changeLanguage, owner: self, options: nil)) {
            if let bundleFirstData = bundle.first {
                if let addLanguageView = bundleFirstData as? ChangeLanguage {
                    addLanguageView.lableChangeLanguageTitle.text = StringConstant.changeLanugageTitle.localizeString()
                    addLanguageView.delegate = self
                    addLanguageView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
                    self.navigationController?.view.addSubview(addLanguageView)
                    addLanguageView.clipsToBounds = false
                    self.view.layoutIfNeeded()
                }
            }
        }
     }
    
    //MARK: Add company Popup View
    private func showPopupViewForAddCompany(){
         
        if let bundle = (Bundle.main.loadNibNamed(Constants.addCompanyView, owner: self, options: nil)) {
            if let bundleFirstData = bundle.first {
                if let addCmpView = bundleFirstData as? AddCompanyView {
                    addCmpView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
                    labelOfEmployeeHeader.text = StringConstant.findAllEmployees.localizeString()
                    addCmpView.textFieldCompanyName.setLeftPaddingPoints(10)
                    addCmpView.delegate = self
                    self.navigationController?.view.addSubview(addCmpView)
                    addCmpView.labelForErrorMessage.isHidden = true
                    addCmpView.textFieldCompanyName.isUserInteractionEnabled = true
                    addCmpView.clipsToBounds = false
                    addCmpView.textFieldCompanyName.placeholder = StringConstant.enterCompanyName.localizeString()
                    addCmpView.buttonAdd.setTitle(StringConstant.add.localizeString(), for: .normal)
                    addCmpView.buttonCancel.setTitle(StringConstant.cancel.localizeString(), for: .normal)
                    self.view.layoutIfNeeded()
                }
            }
        }
     }
    
    //MARK: Add employee Popup View
    private func showPopupViewForAddEmployee() {
        
        if let bundle = (Bundle.main.loadNibNamed(Constants.addEmployeeView, owner: self, options: nil)) {
            if let bundleFirstData = bundle.first {
                if let addEmpView = bundleFirstData as? AddEmployeeView {
                    addEmpView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                    addEmpView.delegate = self
                    addEmpView.labelForValidation.isHidden = true
                    addEmpView.textFieldEmployeeName.setLeftPaddingPoints(10)
                    addEmpView.textFieldEmployeeSalary.setLeftPaddingPoints(10)
                    self.navigationController?.view.addSubview(addEmpView)
                    addEmpView.textFieldEmployeeName.placeholder = StringConstant.placeholderName.localizeString()
                    addEmpView.textFieldEmployeeSalary.placeholder = StringConstant.placeholderSalary.localizeString()
                    addEmpView.labelForValidation.text = StringConstant.pleaseEnterCompanyName.localizeString()
                    addEmpView.buttonAdd.setTitle(StringConstant.add.localizeString(), for: .normal)
                    addEmpView.buttonCancel.setTitle(StringConstant.cancel.localizeString(), for: .normal)
                    self.view.layoutIfNeeded()
                }
            }
        }
     }
    
    //MARK: Filter button action
    @IBAction func buttonFiltersActions(_ sender: UIButton) {
 
        let vc = self.storyboard?.instantiateViewController(withIdentifier: Constants.filterVC) as? FilterVC
        if let vc {
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK: Clear data button action
    @IBAction func buttonClearDataActions(_ sender: UIButton) {
        labelOfEmployeeHeader.text = StringConstant.findAllEmployees.localizeString()
        if self.employeeViewModel.isFilterProcessExectutedByUser {
            self.employeeViewModel.isFilterProcessExectutedByUser = false
            self.employeeViewModel.fetchCompanyRecords()
        }
    }
    
    //MARK: Reload company tableview
    func reloadTableView() {
        self.view.endEditing(true)
        DispatchQueue.main.async {
            self.tableViewEmployee.reloadData()
        }
    }
    
    //MARK: Dismiss Keyboard
    func dismissKeyboard() {
        guard let view = self.view.superview else { return }
        view.endEditing(true)
    }
}
 
//MARK: Employee Delat delegate
extension EmployeeVC: EmployeeCellDelegate {
    func didEmployeeDeleted() {
        self.employeeViewModel.fetchCompanyRecords()
    }
}

//MARK: Section Delegate
extension EmployeeVC: EmployeeHeaderCellDelegate {
    
    func getSelectedCompanySectionValue(sectionValue: Int) {
        self.employeeViewModel.selectedCompanyIndex  = sectionValue
        self.showPopupViewForAddEmployee()
    }
}
 
//MARK: Get Filter Company list with employee data
extension EmployeeVC: FilterVCDelegate {
    func allFilteredCompanyData(filterDataArray: [Company], nameFilterIndex: Int, salaryFilterIndex: Int, name: String, minSalary: Float, maxSalary: Float) {
        
        self.employeeViewModel.isFilterProcessExectutedByUser = true
        self.employeeViewModel.arrayOfFileteredCompany.value = filterDataArray
        self.reloadTableView()
        
        switch (nameFilterIndex, salaryFilterIndex) {
        case (2, 4):
            self.labelOfEmployeeHeader.text = String(format: NSLocalizedString(StringConstant.findEqualNameAndLessSalary.localizeString(), comment: ""), "\(name)" , "\(minSalary)")
            
        case (2, 5):
            self.labelOfEmployeeHeader.text = String(format: NSLocalizedString(StringConstant.findEqualNameAndMoreSalary.localizeString(), comment: ""), "\(name)" , "\(maxSalary)")
            
        case (3, 4):
            self.labelOfEmployeeHeader.text = String(format: NSLocalizedString(StringConstant.findContainsNameAndLessSalary.localizeString(), comment: ""), "\(name)" , "\(minSalary)")
            
        case (3, 5):
            self.labelOfEmployeeHeader.text = String(format: NSLocalizedString(StringConstant.findContainsNameAndMoreSalary.localizeString(), comment: ""), "\(name)" , "\(maxSalary)")
            
        case (2, -1) :
            self.labelOfEmployeeHeader.text = String(format: NSLocalizedString(StringConstant.findAllEmployeeEqualName.localizeString(), comment: ""), "\(name)")
            
        case (3, -1) :
            self.labelOfEmployeeHeader.text = String(format: NSLocalizedString(StringConstant.findAllEmployeeContainsName.localizeString(), comment: ""), "\(name)")
            
        case (-1, 4) :
            self.labelOfEmployeeHeader.text = String(format: NSLocalizedString(StringConstant.findAllEmployeeSalaryLessThan.localizeString(), comment: ""), "\(minSalary == 0 ? maxSalary : minSalary)")
            
        case (-1, 5) :
            self.labelOfEmployeeHeader.text = String(format: NSLocalizedString(StringConstant.findAllEmployeeSalaryMoreThan.localizeString(), comment: ""), "\(maxSalary == 0 ? minSalary : maxSalary)")
        default:
            print("")
        }
    }
}

