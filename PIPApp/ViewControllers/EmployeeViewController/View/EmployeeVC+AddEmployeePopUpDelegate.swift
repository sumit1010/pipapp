//
//  EmployeeVC+AddEmployeePopUpDelegate.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 20/07/23.
//

import Foundation
import UIKit

extension EmployeeVC: AddEmployeeViewDelegate {
    
    func didAddEmployeeData(name: String, salary: Float) {
        self.dismissKeyboard()
        let companyId =  self.employeeViewModel.isFilterProcessExectutedByUser ?  self.employeeViewModel.arrayOfFileteredCompany.value[self.employeeViewModel.selectedCompanyIndex].companyId :
        self.employeeViewModel.arrayOfComapanyData.value[self.employeeViewModel.selectedCompanyIndex].companyId
        let employee = Employee(employeeId: UUID(), employeeName: name, employeeSalary: salary)
        let isUpdated = self.employeeViewModel.manager.updateCompanyData(employeeData: employee, companyId: companyId)
        if isUpdated! {
            self.employeeViewModel.fetchCompanyRecords()
        }
    }
    
    func didCancelEmployee() {
        self.dismissKeyboard()
    }
}
