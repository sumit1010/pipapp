//
//  EmployeeVC+TableViewDataSource.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 20/07/23.
//

import Foundation
import UIKit

extension EmployeeVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.employeeViewModel.isFilterProcessExectutedByUser ?
        employeeViewModel.arrayOfFileteredCompany.value.count :
        employeeViewModel.arrayOfComapanyData.value.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.employeeViewModel.isFilterProcessExectutedByUser ?
        employeeViewModel.arrayOfFileteredCompany.value[section].employeeData.count :
        employeeViewModel.arrayOfComapanyData.value[section].employeeData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.employeeCell, for: indexPath) as! EmployeeCell
        cell.configureCell(employee: self.employeeViewModel.isFilterProcessExectutedByUser ? employeeViewModel.arrayOfFileteredCompany.value[indexPath.section].employeeData[indexPath.row] :
            employeeViewModel.arrayOfComapanyData.value[indexPath.section].employeeData[indexPath.row])
        cell.delegate = self
        return cell
    }
 }
