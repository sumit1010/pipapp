//
//  EmployeeVC+AddCompanyPopUpDelegate.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 23/07/23.
//

import Foundation

extension EmployeeVC: AddCompanyViewDelegate {
    
    func didAddCompanyName() {
        self.dismissKeyboard()
        self.employeeViewModel.fetchCompanyRecords()
    }
    
    func didCancel() {
        self.dismissKeyboard()
     }
    
}
