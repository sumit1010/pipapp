

import Foundation

class EmployeeViewModel {
    
    var isFilterProcessExectutedByUser = false
    var arrayOfComapanyData: EmployeeBinder<[Company]> = EmployeeBinder([])
    var arrayOfFileteredCompany: EmployeeBinder<[Company]> = EmployeeBinder([])
    var selectedCompanyIndex: Int = -1
    var popupViewForAddCompany: AddCompanyView?
    var popupViewForAddEmployee: AddEmployeeView?
    var popupViewForChangeLanguage: ChangeLanguage?
    let manager = CompanyManager()
    
    init() {
        self.fetchCompanyRecords()
    }
    
    func fetchCompanyRecords() {
        let allCompanyData = manager.getSavedCompanyData()
        isFilterProcessExectutedByUser = false
        if let allCompanyData = allCompanyData, allCompanyData.count != 0 {
            arrayOfComapanyData.value = allCompanyData
        }
    }
    
}

