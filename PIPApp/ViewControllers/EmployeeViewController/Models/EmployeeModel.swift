 

import Foundation

class Company
{
    let companyId: UUID
    let companyName: String
    var employeeData: [Employee]
    
    init(companyId: UUID, companyName: String, employeeData: [Employee] = [])
    {
        self.companyId = companyId
        self.companyName = companyName
        self.employeeData = employeeData
    }
}

class Employee
{
    let employeeId: UUID
    let employeeName: String
    var employeeSalary: Float
    
    init(employeeId: UUID, employeeName: String , employeeSalary: Float)
    {
        self.employeeId = employeeId
        self.employeeName = employeeName
        self.employeeSalary = employeeSalary
    }
}
