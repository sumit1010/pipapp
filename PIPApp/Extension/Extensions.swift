//
//  String+Extension.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 25/07/23.
//

import Foundation
import UIKit


extension String {
    func localizeString() -> String {
        let getLanguageType = UserDefaults.retriveLanguage()
        let path = Bundle.main.path(forResource: getLanguageType, ofType: Constants.bundleExtension)
        guard let path = path else { return Constants.emptyString}
        let bundle = Bundle(path: path)
        if let bundle {
            return NSLocalizedString(self, tableName: nil, bundle: bundle, value: Constants.emptyString, comment: Constants.emptyString)
        }
        return Constants.emptyString
    }
}

extension UserDefaults {
    
    static func saveLanguage(lang: String) {
        let userDefaults =  UserDefaults.standard
        userDefaults.set(lang, forKey: Constants.languageKey)
    }
    
    static func retriveLanguage() -> String {
        if let getlanguage = UserDefaults.standard.object(forKey: Constants.languageKey) as? String {
            return getlanguage
        }
        return Constants.emptyString
    }
    
    static func saveDocumentDirectoryPath(path: String) {
        let userDefaults =  UserDefaults.standard
        userDefaults.set(path, forKey: "path")
    }
    
    static func retriveDocumentDirectoryPath() -> String? {
        if let getPath = UserDefaults.standard.object(forKey: "path") as? String {
            return getPath
        }
        return Constants.emptyString
    }

    
    
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
