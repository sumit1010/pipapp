//
//  StringConstant.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 26/07/23.
//

import Foundation


struct Constants {
  
    static let baseUrlForThummnailLoading  = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/"
    
    static let englishLanguage = "en"
    static let spanishLanguage = "es"
    
    static let addImageName = "add"
    static let moreImageName = "more"
    static let uncheckImage = "uncheck"
    static let checkImage = "check"
    static let changeLanguage = "ChangeLanguage"
    static let addCompanyView = "AddCompanyView"
    static let addEmployeeView = "AddEmployeeView"
    static let filterVC = "FilterVC"
    static let employeeCell = "EmployeeCell"
    static let videoCell = "VideoCell"
    static let employeeHeaderCell = "EmployeeHeaderCell"
    static let bundleExtension = "lproj"
    static let languageKey = "language"
    static let emptyString = ""
}

struct StringConstant {
  
    static let add = "Add"
    static let cancel = "Cancel"
    static let cancelFilter = "CancelFilter"
    static let enterCompanyName = "Enter company name"
    static let pleaseEnterCompanyName = "Please enter company name"
    static let emloyeeName = "Employee name"
    static let salary = "Salary"
    static let clearData = "Clear Data"
    static let filters = "Filters"
    static let nameOrPart = "Name or part of the name"
    static let minSalary = "min Salary"
    static let maxSalary = "Max Salary"
    static let noFilter = "No filter"
    static let nameEqualsTo = "Name equals to"
    static let nameContainsTo = "Name contains to"
    static let changeLanugageTitle = "Change language"
    static let findAllEmployees = "Find all employees"
    static let okTitle = "Ok"
    static let salaryLessThan = "SalaryLessThan"
    static let salaryMoreThan = "SalaryMoreThan"
    static let delete = "Delete"
    static let name = "Name"
    static let enterEmployeeName = "EnterEmpName"
    static let enterEmployeeSalary = "EnterEmpSalary"
    static let placeholderName = "PlaceholderName"
    static let placeholderSalary = "PlaceholderSalary"
    static let findAllEmployeeEqualName = "FindAllEmployeesNameEqualsTo"
    static let findAllEmployeeContainsName = "FindAllEmployeesNameContainsTo"
    static let findAllEmployeeSalaryLessThan = "FindAllEmployeesSalaryLess"
    static let findAllEmployeeSalaryMoreThan = "FindAllEmployeesSalaryMoreThan"
    static let findEqualNameAndLessSalary = "FindEqualNameAndLessSalary"
    static let findEqualNameAndMoreSalary = "FindEqualNameAndMoreSalary"
    static let findContainsNameAndLessSalary = "FindContainsNameAndLessSalary"
    static let findContainsNameAndMoreSalary = "FindContainsNameAndMoreSalary"
}


