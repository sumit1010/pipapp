//
//  AddEmployeeViewModel.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 02/08/23.
//

import Foundation


class AddEmployeeViewModel {
    
    var isValidationSuccess: EmployeeBinder<Bool> = EmployeeBinder(false)
    var isEmployeeNameValidation = false
    var isEmployeeSalaryValidation = false
    
    func addEmployee(employeeName: String, salary: String) {
        
        let employeeName = employeeName.trimmingCharacters(in: .whitespaces)
        let employeeSalary = salary.filter {!$0.isWhitespace}
        
        if employeeName.isEmpty {
            isEmployeeNameValidation = false
            isValidationSuccess.value = false
            return
        }
        if employeeSalary.isEmpty {
            isEmployeeNameValidation = true
            isEmployeeSalaryValidation = false
            isValidationSuccess.value = false
            return
        }
        if !employeeName.isEmpty && !employeeSalary.isEmpty {
            isEmployeeSalaryValidation = true
            isEmployeeNameValidation = true
            isValidationSuccess.value = true
            return
        }
    }
}
