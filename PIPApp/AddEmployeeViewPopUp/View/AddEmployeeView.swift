//
//  AddEmployeeView.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 20/07/23.
//

import UIKit

protocol AddEmployeeViewDelegate {
    func didAddEmployeeData(name: String, salary: Float)
    func didCancelEmployee()
}

class AddEmployeeView: UIView {
    @IBOutlet weak var textFieldEmployeeName:UITextField!
    @IBOutlet weak var textFieldEmployeeSalary:UITextField!
    @IBOutlet weak var labelForValidation:UILabel!
    @IBOutlet weak var buttonAdd:UIButton!
    @IBOutlet weak var buttonCancel:UIButton!
    
    var addEmployeeViewModel: AddEmployeeViewModel = AddEmployeeViewModel()
    var delegate: AddEmployeeViewDelegate?
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.bindEmployeeValidation()
    }
    
    @IBAction func buttonAddAction(_sender: UIButton) {
        self.addEmployeeViewModel.addEmployee(employeeName: textFieldEmployeeName.text ?? "", salary: textFieldEmployeeSalary.text ?? "")
     }
    
    
    func bindEmployeeValidation() {
        self.addEmployeeViewModel.isValidationSuccess.bind { [self] isValidationSuccess in
            if isValidationSuccess {
                self.isHidden = true
                self.labelForValidation.isHidden = true
                let salary  = ( self.textFieldEmployeeSalary.text! as NSString).floatValue
                let employeeName =  self.textFieldEmployeeName.text ?? ""
                delegate?.didAddEmployeeData(name: employeeName, salary: salary)
            } else {
                guard let validationLabel = labelForValidation else { return }
                if self.addEmployeeViewModel.isEmployeeNameValidation == false {
                    validationLabel.isHidden = false
                    validationLabel.text = StringConstant.enterEmployeeName.localizeString()
                    return
                }
                if self.addEmployeeViewModel.isEmployeeSalaryValidation == false {
                    validationLabel.isHidden = false
                    validationLabel.text = StringConstant.enterEmployeeSalary.localizeString()
                    return
                }
            }
        }
    }
 
    @IBAction func buttonCancelAction(_sender: UIButton) {
        delegate?.didCancelEmployee()
        self.isHidden = true
    }
}
