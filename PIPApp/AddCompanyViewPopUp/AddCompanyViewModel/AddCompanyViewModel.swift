//
//  AddCompanyViewModel.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 02/08/23.
//

import Foundation

class AddCompanyViewModel {
    
    let manager = CompanyManager()
    var isCompanySaved: EmployeeBinder<Bool> = EmployeeBinder(false)
    var isValidationSuccess: EmployeeBinder<Bool> = EmployeeBinder(false)

    func addCompany(companyId: UUID, companyName: String, employeeData: [Employee]) {
        let isSaved = manager.createPerson(record: Company(companyId: UUID(), companyName: companyName , employeeData: []))
        self.isCompanySaved.value = isSaved
    }
    
    func addValidationForComapany(companyName: String) {
        if companyName != "" {
            let companyName = companyName.filter {!$0.isWhitespace}
            if companyName.isEmpty {
                self.isValidationSuccess.value = false
            } else {
                self.isValidationSuccess.value = true
                let isSaved = manager.createPerson(record: Company(companyId: UUID(), companyName: companyName , employeeData: []))
                self.isCompanySaved.value = isSaved
            }
        }
        self.isValidationSuccess.value = false
     }
}
