

import UIKit

protocol AddCompanyViewDelegate {
    func didAddCompanyName()
    func didCancel()
}

class AddCompanyView: UIView {
    
    @IBOutlet weak var textFieldCompanyName:UITextField!
    @IBOutlet weak var labelForErrorMessage:UILabel!
    @IBOutlet weak var buttonCancel:UIButton!
    @IBOutlet weak var buttonAdd : UIButton!
    var delegate: AddCompanyViewDelegate?
    var addCompanyViewModel: AddCompanyViewModel = AddCompanyViewModel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.bindCompanyValidation()
        self.bindAddCompanyData()
    }
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        self.addCompanyViewModel.addValidationForComapany(companyName: textFieldCompanyName.text ?? "")
    }
    
    func bindAddCompanyData() {
        self.addCompanyViewModel.isCompanySaved.bind { saved in
            if saved {
                self.isHidden = true
                self.delegate?.didAddCompanyName()
            } else {
                debugPrint("")
            }
        }
    }
    
    func bindCompanyValidation() {
        self.addCompanyViewModel.isValidationSuccess.bind { isSuccessValidation in
            guard let label = self.labelForErrorMessage else {return}
            if isSuccessValidation {
                label.isHidden = true
            } else {
                label.isHidden = false
            }
        }
    }
    
    @IBAction func cancelButtonActino (_ sender: UIButton) {
        delegate?.didCancel()
        self.isHidden = true
    }
}

