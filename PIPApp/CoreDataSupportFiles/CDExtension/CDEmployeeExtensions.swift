import Foundation

extension CDEmployee {
    func convertToEmployee() -> Employee {
        return Employee(employeeId: self.employeeId!, employeeName: self.name ?? "", employeeSalary: self.salary)
    }
}
