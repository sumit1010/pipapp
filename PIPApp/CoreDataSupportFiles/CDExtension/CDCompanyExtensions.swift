

import Foundation

extension CDCompany {
    
    func convertToCompany() -> Company {
        return Company(companyId: self.id ?? UUID(), companyName: self.companyName ?? "", employeeData: self.convertToEmployee() ?? [])
    }
    
    private func convertToEmployee() -> [Employee]? {
        guard self.toEmployee != nil && self.toEmployee?.count != 0 else { return  [] }
        var employees: [Employee] = []
        self.toEmployee?.forEach({ (cdEmployee) in
            employees.append(cdEmployee.convertToEmployee())
        })
        return employees
    }
    
    private func convertToEmployeeWithFilterData() -> [Employee]? {
        guard self.toEmployee != nil && self.toEmployee?.count != 0 else { return  [] }
        var employees: [Employee] = []
        
        self.toEmployee?.forEach({ (cdEmployee) in
            employees.append(cdEmployee.convertToEmployee())
        })
        return employees
    }
    
}
