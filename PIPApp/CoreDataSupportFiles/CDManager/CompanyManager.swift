
import Foundation

struct CompanyManager {
    
    private let companyDataManager = CompanyDataManager()
    
    func createPerson(record: Company) -> Bool {
        companyDataManager.createEmployee(record: record)
        return true
    }
    
    func getSavedCompanyData() -> [Company]? {
        return companyDataManager.getAllCompanyRecords()
    }
    
    func updateCompanyData(employeeData: Employee, companyId: UUID) -> Bool? {
        return companyDataManager.updateComapanyRecord(employeeData: employeeData, companyId: companyId)
    }
    
    
    func deleteEmployeeWith(id: UUID) -> Bool {
        return companyDataManager.deleteEmployee(employeeId: id)
    }
    
    //    func fetchFiteredDataFromDB(nameOrPart: String,
    //                                minSalary: String,
    //                                maxSalary: String ,
    //                                iSNameContainsTo: Bool,
    //                                isSalaryMinMax: Bool) -> [Company]? {
    //        return companyDataManager.filterCompanyData(nameOrPart: nameOrPart, minSalary: minSalary, maxSalary: maxSalary, iSNameContainsTo: iSNameContainsTo, isSalaryMinMax: isSalaryMinMax)
    //    }
    
    
    
    //    func getAllFiteredCompanyRecord(filterParam: String, filterTypeNo: Int, filterTypeNameValue: Int, filterTypeSalaryValue: Int) -> [Company]? {
    //        return companyDataManager.getAllFilterCompanyRecords(filterParam: filterParam, filterTypeNo: filterTypeNo, filterTypeNameValue: filterTypeNameValue, filterTypeSalaryValue: filterTypeSalaryValue)
    //     }
    
    
}
