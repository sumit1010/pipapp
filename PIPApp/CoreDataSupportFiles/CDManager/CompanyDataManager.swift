
import Foundation
import CoreData


struct CompanyDataManager  {
    
    //MARK: Create Company with employee in DB
    func createEmployee(record: Company) {
        
        let cdCompany = CDCompany(context: PersistentStorage.shared.context)
        cdCompany.companyName = record.companyName
        cdCompany.id = UUID()
        if(record.employeeData.count != 0)
        {
            var employeeSet = Set<CDEmployee>()
            record.employeeData.forEach({ (emp) in
                let cdEmplyee = CDEmployee(context: PersistentStorage.shared.context)
                cdEmplyee.employeeId = UUID()
                cdEmplyee.name = emp.employeeName
                cdEmplyee.salary = emp.employeeSalary
                employeeSet.insert(cdEmplyee)
            })
            cdCompany.toEmployee = employeeSet
        }
        PersistentStorage.shared.saveContext()
    }
    
    //MARK: Get All Recoreds from DB
    func getAllCompanyRecords() -> [Company] {
        
        let records = PersistentStorage.shared.fetchManagedObject(managedObject: CDCompany.self)
        guard records != nil && records?.count != 0 else { return [] }
        var results: [Company] = []
        if let records = records {
            records.forEach({ (cdCompany) in
                results.append(cdCompany.convertToCompany())
            })
            return results
        }
        return []
    }
    
    
    //MARK: update Recoreds from DB
    func updateComapanyRecord(employeeData: Employee, companyId: UUID) -> Bool {
        let context = PersistentStorage.shared.context
        do {
            let fetchRequest = NSFetchRequest<CDCompany>(entityName: "CDCompany")
            fetchRequest.predicate = NSPredicate(format: "id == %@", companyId as CVarArg)
            let fetchedResults = try context.fetch(fetchRequest)
            if let company = fetchedResults.first {
                let employee = CDEmployee.init(context: PersistentStorage.shared.context)
                employee.employeeId = employeeData.employeeId
                employee.salary = employeeData.employeeSalary
                employee.name = employeeData.employeeName
                company.toEmployee?.insert(employee)
            }
        }
        catch {
            print ("fetch task failed", error)
            return false
        }
        PersistentStorage.shared.saveContext()
        return true
    }
    
    
    //MARK: Delete Recoreds from DB
    func deleteEmployee(employeeId: UUID) -> Bool {
        let context = PersistentStorage.shared.context
        do {
            let fetchRequest = NSFetchRequest<CDEmployee>(entityName: "CDEmployee")
            fetchRequest.predicate = NSPredicate(format: "employeeId == %@", employeeId as CVarArg)
            let fetchedResults = try context.fetch(fetchRequest)
            if fetchedResults.count != 0 {
                if let empDetails = fetchedResults.first {
                    PersistentStorage.shared.context.delete(empDetails)
                }
            }
        }
        catch {
            print ("fetch task failed", error)
            return false
        }
        PersistentStorage.shared.saveContext()
        return true
    }
 }
