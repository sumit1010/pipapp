//
//  ChangeLanguageViewModel.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 02/08/23.
//

import Foundation


class ChangeLanguageViewModel {
    
    var languageName: EmployeeBinder<String> = EmployeeBinder("")

    func changeLanguage(language: String) {
        UserDefaults.saveLanguage(lang: language)
        languageName.value = UserDefaults.retriveLanguage()
    }
}
