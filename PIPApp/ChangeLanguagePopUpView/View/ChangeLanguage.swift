//
//  ChangeLanguage.swift
//  PIPApp
//
//  Created by Sumit Vishwakarma on 26/07/23.
//

import UIKit

protocol ChangeLanguageDelegate {
    func didChangeLanguage()
}

class ChangeLanguage: UIView {
    
    @IBOutlet weak var lableChangeLanguageTitle:UILabel!
    var delegate :ChangeLanguageDelegate?
    var changeLanguageViewModel : ChangeLanguageViewModel = ChangeLanguageViewModel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.bindLanguageToBeChanged()
    }
    
    @IBAction func buttonEnglishlAction(_sender: UIButton) {
        self.changeLanguageViewModel.changeLanguage(language: Constants.englishLanguage)
    }
    
    @IBAction func buttonSpnishAction(_sender: UIButton) {
        self.changeLanguageViewModel.changeLanguage(language: Constants.spanishLanguage)
    }
    
    func bindLanguageToBeChanged() {
        self.changeLanguageViewModel.languageName.bind { language in
            if !language.isEmpty {
                self.lableChangeLanguageTitle.text = language == Constants.englishLanguage ? StringConstant.changeLanugageTitle.localizeString() :StringConstant.changeLanugageTitle.localizeString()
                self.isHidden = true
                self.delegate?.didChangeLanguage()
            }
            
         }
    }
    
}
