 
import Foundation
import CoreData


extension CDEmployee {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDEmployee> {
        return NSFetchRequest<CDEmployee>(entityName: "CDEmployee")
    }

    @NSManaged public var name: String?
    @NSManaged public var salary: Float
    @NSManaged public var employeeId: UUID?
    @NSManaged public var toCompany: CDCompany?

}

extension CDEmployee : Identifiable {

}
