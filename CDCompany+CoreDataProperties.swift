import Foundation
import CoreData


extension CDCompany {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDCompany> {
        return NSFetchRequest<CDCompany>(entityName: "CDCompany")
    }

    @NSManaged public var companyName: String?
    @NSManaged public var id: UUID?
    @NSManaged public var toEmployee: Set<CDEmployee>?

}

// MARK: Generated accessors for toEmployee
extension CDCompany {

    @objc(addToEmployeeObject:)
    @NSManaged public func addToToEmployee(_ value: CDEmployee)

    @objc(removeToEmployeeObject:)
    @NSManaged public func removeFromToEmployee(_ value: CDEmployee)
 
    @objc(addToVehicle:)
    @NSManaged public func addToToVehicle(_ values: Set<CDEmployee>)

    @objc(removeToVehicle:)
    @NSManaged public func removeFromToVehicle(_ values: Set<CDEmployee>)

}

extension CDCompany : Identifiable {

}
